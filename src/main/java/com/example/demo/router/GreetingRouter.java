package com.example.demo.router;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.handler.GreetingHandler;
import com.example.demo.model.Greeting;

@Configuration(proxyBeanMethods = false)
public class GreetingRouter {

	@Autowired
	GreetingHandler handler;

	@Bean
	public RouterFunction<ServerResponse> greetingRoute(GreetingHandler greetingHandler) {
		return route(GET("/all"), req -> ok().body(handler.getAll(), Greeting.class))
				.and(route(GET("/hello"), req -> ok().body(handler.hello(req), Greeting.class)));
	}

}
