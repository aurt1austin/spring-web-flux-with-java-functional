package com.example.demo.handler;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import com.example.demo.model.Greeting;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class GreetingHandler {

	@Autowired
	private GreetingRepo repo;

	public Mono<Greeting> hello(ServerRequest request) {
		Greeting newBean = new Greeting();
		newBean.setMessage("xx");
		return repo.save(newBean);
	}

	public Flux<Greeting> getAll() {
		return repo.findAll();
	}

}
